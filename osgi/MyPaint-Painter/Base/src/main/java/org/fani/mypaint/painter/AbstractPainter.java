package org.fani.mypaint.painter;

import com.sun.corba.se.impl.orbutil.graph.Graph;
import org.fani.mypaint.painter.api.Painter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public abstract class AbstractPainter implements Painter {

    private static JButton button;

    public JButton createButton() {
        System.out.println("Create button");

        if (button == null) {
            System.out.println("Create button not null");

            URL imageURL = getImageURL();

            button = new JButton();
            String altText = getClass().getName();

            if (imageURL != null) {
                button.setIcon(new ImageIcon(imageURL, altText));
            } else {
                button.setText(altText);
                System.err.println("Resource not found: " + imageURL);
            }

//            throw new RuntimeException("Woi~!");
        }

        return button;
    }

    protected URL getImageURL() {
        return getClassForResource().getResource(getImageLocation());
    }

    protected void drawImage(Graphics g, int x, int y) {
        BufferedImage img;
        try {
            img = ImageIO.read(getImageURL());
            g.drawImage(img, x, y, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected abstract String getImageLocation();
    protected abstract Class getClassForResource();
    public abstract void draw(Graphics g, int x, int y);

}
