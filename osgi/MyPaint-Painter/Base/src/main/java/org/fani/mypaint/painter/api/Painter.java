package org.fani.mypaint.painter.api;

import javax.swing.*;
import java.awt.*;

public interface Painter {

    public JButton createButton();
    public abstract void draw(Graphics g, int x, int y);

}
