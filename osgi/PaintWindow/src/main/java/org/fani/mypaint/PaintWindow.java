package org.fani.mypaint;

import org.fani.mypaint.painter.api.Painter;

public interface PaintWindow {

    public void registerPainter(Painter painter);
    public void unregisterPainter(Painter painter);

}
