import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * Created by sebastianvincent on 12/2/14.
 */
@ManagedBean(name="counter")
public class CounterBean {
    private int count = 0;
    private String text = "Hello";
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
    public void increment() {
        count++;
    }

    public void setText(String text){
        this.text = text;
    }

    public String getText() {
        return text;
    }
}


