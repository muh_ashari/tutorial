import javax.faces.bean.ManagedBean;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

@ManagedBean(name="bean")
public class Bean {
    public Bean(){
        Date date = new Date();
        setDate(date);
    }
    private String text;
    private Date date;
    public List<String> complete(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < 10; i++)
            results.add(query + i);
        return results;
    }
    //getter setter
    public String getText(){
        return text;
    }
    public void setText(String text){
        this.text=text;
    }

    public Date getDate(){
        return date;
    }

    public void setDate(Date date){
        this.date=date;
    }
}