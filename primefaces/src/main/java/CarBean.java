/**
 * Created by sebastianvincent on 12/2/14.
 */
import javax.faces.bean.ManagedBean;
import java.util.List;
import java.util.ArrayList;

@ManagedBean(name="carBean")
public class CarBean {
    private List<Car> cars;
    public CarBean() {
        cars = new ArrayList<Car>();
        cars.add(new Car("Honda", 2005, "Honda", "blue"));
        cars.add(new Car("Mitsubishi", 2005, "Mitsubishi", "green"));
        //add more cars
    }
    //getter setter
    public void setCars(List<Car> cars){
        this.cars = cars;
    }
    public List<Car> getCars(){
        return cars;
    }
}
