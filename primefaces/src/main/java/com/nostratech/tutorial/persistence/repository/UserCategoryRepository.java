package com.nostratech.tutorial.persistence.repository;

import com.nostratech.tutorial.persistence.domain.UserCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCategoryRepository extends JpaRepository<UserCategory, Integer> {

}
