package com.nostratech.tutorial.persistence.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name="USER")
public class User extends Base {
	
	@JoinColumn(name="CATEGORY_ID")
	@ManyToOne(targetEntity=UserCategory.class, fetch=FetchType.LAZY)
	private UserCategory categoryId;
	
	@Column(name="USERNAME", unique=true)
	private String userName;

	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="PASSWORD")
	private String password;
	
	@Column(name="PHONE", nullable=true)
	private String phone;
	
	@Column(name="LOCATION")
	private String location;
	
	@Column(name="SEX", length=1)
	private String gender;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name="BIRTHDAY")
	private Date birthday;
	
	@Column(name="PHOTO_URL")
	private String photoUrl;
	
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
		@JoinTable(name = "FOLLOWER",
		joinColumns = {
				@JoinColumn(name="USER_ID") 
		},
		inverseJoinColumns = {
				@JoinColumn(name="FOLLOWER_ID")
		}
	)
	private Set<User> follower;
	
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
		@JoinTable(name = "FOLLOWING",
		joinColumns = {
				@JoinColumn(name="USER_ID") 
		},
		inverseJoinColumns = {
				@JoinColumn(name="FOLLOWING_ID")
		}
	)
	private Set<User> following;
	
	@OneToMany(targetEntity=Car.class, fetch=FetchType.LAZY, mappedBy="owner", cascade=CascadeType.ALL)
	private Set<Car> cars;
	
	@Transient
	private Integer countFollower;
	
	@Transient
	private Integer countFollowing;

	public User() {
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public UserCategory getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(UserCategory categoryId) {
		this.categoryId = categoryId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Set<User> getFollower() {
		return follower;
	}

	public void setFollower(Set<User> follower) {
		this.follower = follower;
	}
	
	public Set<User> getFollowing() {
		return following;
	}

	public void setFollowing(Set<User> following) {
		this.following = following;
	}

	public Set<Car> getCars() {
		return cars;
	}

	public void setCars(Set<Car> cars) {
		this.cars = cars;
	}
	
	public Integer getCountFollower() {
		return countFollower;
	}

	public void setCountFollower(Integer countFollower) {
		this.countFollower = countFollower;
	}

	public Integer getCountFollowing() {
		return countFollowing;
	}

	public void setCountFollowing(Integer countFollowing) {
		this.countFollowing = countFollowing;
	}

	@Override
	public String toString() {
		return String.valueOf(id);
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		
		return true;
	}

	
	
}
