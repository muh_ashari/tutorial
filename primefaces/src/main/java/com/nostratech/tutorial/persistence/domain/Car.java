package com.nostratech.tutorial.persistence.domain;


import javax.persistence.*;

@Entity
@Table(name = "CAR")
public class Car extends Base {

    @Column(name = "MAKE")
    private String maker;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "YEAR")
    private String year;

    @Column(name = "ENGINE_CAPACITY")
    private String engineCapacity;

    @Column(name = "TRANSMISSION")
    private String transmission;

    @Column(name = "SEAT_CAPACITY")
    private Number seatCapacity;

    @Column(name = "DOORS")
    private Number doors;

    @Column(name = "PAINT_MODEL")
    private String paintModel;

    @Column(name = "PAINT_MERK")
    private String paintMerk;

    @Column(name = "RIMS_MODEL")
    private String rimsModel;

    @Column(name = "RIMS_MERK")
    private String rimsMerk;

    @Column(name = "MUFFLER_MODEL")
    private String mufflerModel;

    @Column(name = "MUFFLER_MERK")
    private String mufflerMerk;

    @Column(name = "AUDIO_MODEL")
    private String audioModel;

    @Column(name = "AUDIO_MERK")
    private String audioMerk;

    @JoinColumn(name = "USER_ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    private User owner;

    public Car() {
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(String engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public Number getSeatCapacity() {
        return seatCapacity;
    }

    public void setSeatCapacity(Number seatCapacity) {
        this.seatCapacity = seatCapacity;
    }

    public Number getDoors() {
        return doors;
    }

    public void setDoors(Number doors) {
        this.doors = doors;
    }

    public String getPaintModel() {
        return paintModel;
    }

    public void setPaintModel(String paintModel) {
        this.paintModel = paintModel;
    }

    public String getPaintMerk() {
        return paintMerk;
    }

    public void setPaintMerk(String paintMerk) {
        this.paintMerk = paintMerk;
    }

    public String getRimsModel() {
        return rimsModel;
    }

    public void setRimsModel(String rimsModel) {
        this.rimsModel = rimsModel;
    }

    public String getRimsMerk() {
        return rimsMerk;
    }

    public void setRimsMerk(String rimsMerk) {
        this.rimsMerk = rimsMerk;
    }

    public String getMufflerModel() {
        return mufflerModel;
    }

    public void setMufflerModel(String mufflerModel) {
        this.mufflerModel = mufflerModel;
    }

    public String getMufflerMerk() {
        return mufflerMerk;
    }

    public void setMufflerMerk(String mufflerMerk) {
        this.mufflerMerk = mufflerMerk;
    }

    public String getAudioModel() {
        return audioModel;
    }

    public void setAudioModel(String audioModel) {
        this.audioModel = audioModel;
    }

    public String getAudioMerk() {
        return audioMerk;
    }

    public void setAudioMerk(String audioMerk) {
        this.audioMerk = audioMerk;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

}
