package com.nostratech.tutorial.persistence.repository;

import com.nostratech.tutorial.persistence.domain.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Integer> {

}
