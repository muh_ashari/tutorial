package com.nostratech.tutorial.persistence.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * @version \$Revision: 283 $ \$Date: 2012-08-18 12:53:13 +0200 (Sat, 18 Aug 2012) $
 */
@MappedSuperclass
public abstract class Base {

    Base () {

    }

    /**
     * TODO : sequence table generator is for any database, if oracle please use oracle sequence strategy
     */
    @TableGenerator(name = "SEQ_GENERATOR", table = "SEQUENCE_TABLE", pkColumnName = "SEQ_NAME",
            valueColumnName = "SEQ_COUNT", pkColumnValue = "BASE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "SEQ_GENERATOR")
    protected Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_LAST_MODIFIED")
    private Date modificationDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_CREATED")
    private Date creationDate;

    @Version
    @Column(name = "VERSION")
    private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public long getModificationTime() {
        return getModificationDate().getTime();
    }

    public void setModificationTime(long modificationTime) {
        setModificationDate(new Date(modificationTime));
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (this.equals(obj))
            return true;
        if (getClass() != obj.getClass())
            return false;
        Base other = (Base) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}


